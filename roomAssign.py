import requests
import asyncio

""" 
    Return a student object that matches the given W Number. JSON student data can be given through a 
    working http get response. Returns None if there are no students with the given W Number. 
"""
async def getStudent(wNumber, response=None):
    if response is None:
        response = requests.get('https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/students')
        await checkHTTPResponse(response)

    students = response.json()

    for student in students:
        if student["wnumber"] == wNumber:
            return student

    return None


"""
    Returns a list with each room assignment that matches the given student id. JSON student data can be given
    through a working http get response.  Returns None if there are no matching room assignments.
"""


async def getAssignments(studentID, response=None):
    if response is None:
        response = requests.get('https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/assignments')
        await checkHTTPResponse(response)

    assignments = response.json()
    studentAssigns = []
    for assign in assignments:
        if assign["student_id"] == studentID:
            studentAssigns.append(assign)

    if len(studentAssigns) == 0:
        return None
    else:
        return studentAssigns


"""
    Returns the specific room that matches the given room id. JSON data
     can be given through a working http get response. Return None if there are no matches.
"""


async def getRoom(id, response=None):
    if response is None:
        response = requests.get('https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/rooms')
        await checkHTTPResponse(response)

    rooms = response.json()
    for room in rooms:
        if room["id"] == id:
            return room
    return None


""" 
    Returns a dictionary of assignment details 
    from the given wNumber, quarter, and year
"""


async def getAssignmentDetails(wNumber, quarter, year):

    # HTTP GET all the responses from the REST API
    studentResp, roomResp, assignResp = await getResponses()


    # getStudent can run without any dependencies from other functions
    student = await getStudent(wNumber, studentResp)
    if student is None:
        return None

    # getAssign relies on the student id
    assignments = await getAssignments(student["id"], assignResp)
    if assignments is None:
        return None

    room = []
    for a in assignments:
        if a["quarter"] == quarter and a["year"] == year:
            # getRoom relies on assignment room id
            room = await getRoom(a["room_id"], roomResp)

    if len(room) == 0:
        return None

    fullName = student["first"] + " " + student["last"]
    assign_details = createAssignDetails(wNumber, fullName, room["building"], room["room_number"])
    return assign_details




async def getResponses():
    urls = ['https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/students',
            'https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/rooms',
            'https://8a06e5a0-4236-44e3-a74c-65bf2df06849.mock.pstmn.io/assignments']

    responses = await asyncio.gather(
        createResponse(urls[0]),
        createResponse(urls[1]),
        createResponse(urls[2])
    )
    for response in responses:
        await checkHTTPResponse(response)

    return responses

async def createResponse(url):
    return requests.get(url)
""" Creates and assigns values to the Assign Details dictionary.
    Returns a dictionary.
"""


def createAssignDetails(wNumber, fullName, building, roomNumber):
    assignDetails = {"wNumber": wNumber,
                    "fullName": fullName,
                    "building": building,
                    "room": roomNumber
                    }
    return assignDetails




"""
    Checks to see if a HTTP response is valid. If it is not, throw and error.
"""


async def checkHTTPResponse(response):
    if 599 >= response.status_code >= 400:
        raise requests.exceptions.HTTPError("HTTP Address is invalid")


loop = asyncio.get_event_loop()
print(loop.run_until_complete(getAssignmentDetails("W86753099", 'Fall', 2017)))
